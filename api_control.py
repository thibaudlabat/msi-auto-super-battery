"""
00000000  e9 03 00 00 00 12 7b 22  46 61 6e 22 3a 30 2c 22   ......{" Fan":0,"
00000010  49 6e 64 65 78 22 3a 32  2c 22 49 73 4c 6f 61 64   Index":2 ,"IsLoad
00000020  22 3a 66 61 6c 73 65 2c  22 4b 42 22 3a 2d 31 2c   ":false, "KB":-1,
00000030  22 50 42 22 3a 2d 31 2c  22 50 65 72 66 6f 72 6d   "PB":-1, "Perform
00000040  61 6e 63 65 22 3a 33 7d                            ance":3} 
    00000000  31                                                 1


......
{"Fan":0,"Index":2,"IsLoad":false,"KB":-1,"PB":-1,"Performance":3}1

port 32682

{"Fan":0,"Index":4,"IsLoad":false,"KB":-1,"PB":-1,"Performance":3}1


e9 03 00 00 00 12 7b 22  46 61 6e 22 3a 30 2c 22
49 6e 64 65 78 22 3a 32  2c 22 49 73 4c 6f 61 64
22 3a 66 61 6c 73 65 2c  22 4b 42 22 3a 2d 31 2c
22 50 42 22 3a 2d 31 2c  22 50 65 72 66 6f 72 6d
61 6e 63 65 22 3a 33 7d 31
"""

import socket


def set_mode(mode:bytes):
    # mode = 2 : équilibré
    # mode = 4 : super batterie

    HOST="127.0.0.1"
    PORT=32682
    A="""e9 03 00 00 00 12""" # prefix ; this is some magic code
    then = b'{"Fan":0,"Index":'+ mode +b',"IsLoad":false,"KB":-1,"PB":-1,"Performance":3}'

    A=A.replace("\n"," ").replace("  "," ").split(" ")
    B=""
    for x in A:
        print(x,"0x"+x,int("0x"+x,16),chr(int("0x"+x,16)))
        B += chr(int("0x"+x,16))
    C = B.encode("latin")
    C += then

    print("send:",C)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    s.sendall(C)
    data=s.recv(1024)
    print("recv:",data)
    s.close()

set_mode(b"2") # balanced
#set_mode(b"4") # super battery