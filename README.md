# MSI Auto Super Battery

This tool automatically enables the MSI "Super Battery" energy mode, that heavily throttles the CPU, when the computer has not been used (ie. moved the mouse or used the keyboard) for longer than the defined timeout.

I created this tool because my MSI laptop overheats, and it helps.

## Release

[Artifacts page](https://gitlab.com/thibaudlabat/msi-auto-super-battery/-/artifacts)

## Screenshot

![app screenshot](screenshot.png)

## How does it work (reverse engineering)

I decompiled MSI's Dragon Center (C#, decompiled with JetBrains Dot Peek).

It sends the power mode commands to a MSI service on port 32682. (dumped with Wireshark)

The format is JSON. Example for performance mode '2' (balanced): 

`{"Fan":0,"Index":2,"IsLoad":false,"KB":-1,"PB":-1,"Performance":3}`

To change to performance mode '4' (super battery), change `"Index":2` to `"Index":4`.

See `api_control.py` for a standalone Python script that just sends the command to the service.