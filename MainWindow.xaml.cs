﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using System.Net;
using System.ComponentModel;


namespace MSIAutoSuperBattery
{
    public class IdleTime
    {
        [System.Runtime.InteropServices.DllImport("User32.dll"
          )]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO lastInput);

        [StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct LASTINPUTINFO
        {
            public Int32 cbSize;
            public Int32 dwTime;
        }

        public static double getLastInputTime()
        {
            LASTINPUTINFO lastInput = new LASTINPUTINFO();

            lastInput.cbSize = Marshal.SizeOf(lastInput);

            if (GetLastInputInfo(ref lastInput))
            {
                return (Environment.TickCount - lastInput.dwTime) / (double)1000;
            }


            return 0;  // erreur
        }

    }


    public class MyViewModel
    {
        //Standard INotifyPropertyChanged implementation, pick your favorite

        private String myTextValue;
        public String MyTextValue
        {
            get { return myTextValue; }
            set
            {
                myTextValue = value;
            }
        }

    }

    public partial class MainWindow : Window
    {

        double TIMEOUT = 5;

        const string HOST = "127.0.0.1";
        const int PORT = 32682;

        const int BATTERY_MODE_ID = 4;
        const int BALANCED_MODE_ID = 2;

        System.Windows.Forms.NotifyIcon nIcon = new System.Windows.Forms.NotifyIcon();
        private System.Threading.Timer timer;

        System.Drawing.Icon fireIcon = Properties.Resources.fire;
        System.Drawing.Icon waterIcon = Properties.Resources.water;

        Boolean active = true;
        private MyViewModel myViewModel;
        private bool preventClose = true;

        public MainWindow()
        {

            InitializeComponent();

            this.myViewModel = new MyViewModel();
            DataContext = this.myViewModel;

            myViewModel.MyTextValue = TIMEOUT.ToString();


            nIcon.Icon = fireIcon;
            nIcon.Visible = true;
            //nIcon.ShowBalloonTip(5000, "Title", "Text", System.Windows.Forms.ToolTipIcon.Info);
            nIcon.Click += nIcon_Click;

            this.timer = new System.Threading.Timer(Tick, null, 100, 1000);

        }

        void nIcon_Click(object sender, EventArgs e)
        {
            this.Visibility = Visibility.Visible;
            this.WindowState = WindowState.Normal;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if(preventClose)
                e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        public static List<byte> StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToList();
        }

        private async Task setModeAsync(int mode)
        {
            List<byte> data = StringToByteArray("e90300000012")
           .Concat(Encoding.ASCII.GetBytes("{\"Fan\":0,\"Index\":"))
            .Concat(Encoding.ASCII.GetBytes(mode.ToString()))
            .Concat(Encoding.ASCII.GetBytes(",\"IsLoad\":false,\"KB\":-1,\"PB\":-1,\"Performance\":3}"))
            .ToList();

            byte[] buffer = data.ToArray();

            IPAddress ipAddress = System.Net.IPAddress.Parse(HOST);
            IPEndPoint remoteEP = new(ipAddress, PORT);

            if (remoteEP != null)
            {
                using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
                {
                    socket.SendTimeout = 1000;
                    socket.ReceiveTimeout = 1000;
                    socket.Connect((EndPoint)remoteEP);
                    socket.Send(buffer);
                    //byte[] buffer2 = new byte[C_Client.BufferSize];
                    //int num = socket.Receive(buffer2);
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                    socket.Dispose();
                }
            }
        }

        public void Tick(object stateInfo)
        {
            //MessageBox.Show("Running");
            this.Dispatcher.Invoke(() =>
            {
                double t = IdleTime.getLastInputTime();
               if(t>TIMEOUT && active)
                {
                    active = false;
                    nIcon.Icon = waterIcon;

                    setModeAsync(BATTERY_MODE_ID).Wait();
                }
               else if(t<TIMEOUT && !active)
                {
                    active = true;
                    nIcon.Icon = fireIcon;
                    setModeAsync(BALANCED_MODE_ID).Wait();
                }
            });
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            double result;
            var s = myViewModel.MyTextValue;
            if (!Double.TryParse(s, out result))
            {
                MessageBox.Show("Error : invalid timeout : "+ s);
                return;
            }

            TIMEOUT = result;

        }

        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            preventClose = false;
            Close();
        }
    }
}